#!/bin/sh
HOSTNAME=`hostname`
VERSION="2"
ATUALIZACAO=`date`

cat <<EOF > /usr/share/nginx/html/index.html
<HTML>
<HEAD>
<TITLE>$HOSTNAME</TITLE>
</HEAD><BODY>
<H1>Container ID: $HOSTNAME</H1>
<H2>Versao atual: $VERSION</H2>
<H3>Versao Tag: 0.18</H3>
<H3>$ATUALIZACAO</H3>
</BODY>
</HTML>
EOF

nginx -g "daemon off;"
